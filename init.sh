#!/bin/bash

branchName="develop"

if [ -d exp-publisher ]; then
    echo "experiment publisher already present"
    (cd exp-publisher; git checkout $branchName; git pull origin $branchName)
else
    git clone -b $branchName https://gitlab.com/vlead-projects/experiments/infra/publish/exp-publisher.git
    (cd exp-publisher)
fi

if [ -L pub-make ]; then
    echo "symlinked makefile already present"
else 
    ln -sf exp-publisher/makefile pub-make
fi
