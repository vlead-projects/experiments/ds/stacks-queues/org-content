#+TITLE: Stacks and Queues Experiment
#+AUTHOR: VLEAD
#+DATE: [2018-05-15 Tue]
#+TAGS: pmbl lu task txta 
#+EXCLUDE_TAGS: 
#+OPTIONS: ^:nil' prop:t

* Experiment Id                                                       :expId:
:PROPERTIES:
:SCAT: expId
:CUSTOM_ID: stacks-and-queues
:END:

* Type of Document                                                  :docType:
:PROPERTIES:
:SCAT: docType
:CUSTOM_ID: A1
:END:

* Preamble 							       :pmbl:
:PROPERTIES:
:SCAT: pmbl
:CUSTOM_ID: a1-intro
:END:

** Structure of the Experiment 					       :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: a1-exp-struct
:END:

*** Basic Introduction 						       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-basic-intro
:END:
Greeting the user and basic introduction to the experiment.

*** Structure 							       :reqa:
:PROPERTIES:
:SCAT: reqa
:CUSTOM_ID: a1-struct
:END:      
An image which shows the structure of the experiment.

** Learning objectives 						       :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: a1-learning-obj
:END:

*** Objectives 							       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-obj
:END:
Outline learning objectives of stacks and queues experiment.

** Prerequisites 						       :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: a1-pre-req
:END:

*** List of Prerequisites 					       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-pre-req-list
:END:
List out the prerequisites.

* Pre-test 								 :lu:
:PROPERTIES:
:SCAT: lu
:CUSTOM_ID: a1-pre
:END:
  
** Test 							       :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: a1-pre-test
:END:

*** Questions 							       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-pre-test-q-txt
:END:
Five questions for the pre-test to test prior knowledge required.
Questions should cover topics like arrays, linked lists, time
complexity and space complexity.

* Introduction to Stacks 						 :lu:
:PROPERTIES:
:SCAT: lu
:CUSTOM_ID: a1-stacks-intro
:END:

** Real Life Example Of Stacks 					       :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: a1-stacks-example
:END:

*** Image of a Stack of Books 					       :imga:
:PROPERTIES:
:SCAT: imga
:URL: https://images.pexels.com/photos/51342/books-education-school-literature-51342.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260 
:CAPTION: Stack of books 
:CUSTOM_ID: a1-stacks-example-image
:END:
An image of stacks of books to visually represent stacks data structure.

*** Stack of Books 						       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-books-stack-example
:REFERENCE_ID: a1-stacks-example-images
:END:
Explanation of example of stack of books and basic operations on it.

** Stacks Representation and Operations 			       :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: a1-stacks-rep-and-op
:END:

*** Stacks Data Structure 					       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-stacks-ds
:END:
Introduction to stacks as a data structure.

*** Stacks Data Structure Image 				       :reqa:
:PROPERTIES:
:SCAT: reqa
:TYPE: Image
:CUSTOM_ID: a1-stacks-ds-image
:REFERENCE_ID: a1-stacks-ds
:END:
An image of stacks as a data structure.
 
*** Stack Operations 						       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-stacks-ops
:END:
Explanation of stack data structure, stack operations and LIFO. Show/mention how only the top element is accessible.

*** Stack operations image                                             :reqa:
:PROPERTIES:
:SCAT: reqa
:TYPE: Image
:CUSTOM_ID: a1-stacks-ops-image
:REFERENCE_ID: a1-stacks-ops
:END:
Images of push and pop operations.

*** Stack Operations practice                                          :reqa:
:PROPERTIES:
:SCAT: reqa
:TYPE: InteractiveJS
:CUSTOM_ID: a1-interactive-stack-ops-js
:END:
Practice for the user to push and pop.

*** Stack Operations exercise 					       :reqa:
:PROPERTIES:
:SCAT: reqa
:TYPE: InteractiveJS
:CUSTOM_ID: a1-interactive-stack-ops-exercise-js
:END:
Exercise to test the user.

* Implementation of Stacks Using Arrays 				 :lu:
:PROPERTIES:
:SCAT: lu
:CUSTOM_ID: a1-stacks-implement-array
:END:
  
** Arrays 							       :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: a1-stacks-implement-arrays
:END:

*** Description 						       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-stacks-implement-array-des   
:END:
Talk about stacks implemented using arrays, and if stacks overflows or underflows. 

*** Interactive Stack 						       :reqa:
:PROPERTIES:
:SCAT: reqa
:TYPE: InteractiveJS
:CUSTOM_ID: a1-interactive-stack-array-js
:END:
An empty stack implemented as an array (with size as entered by user)
is shown with array indices. Buttons to push, pop etc.

** Time Complexity 						       :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: a1-interactive-stacks-array-time
:END:

*** Time complexity 						       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-interactive-stacks-array-time-compl
:END:
Time complexity for insertion, deletion, search.

* Implementation of Stacks Using Linked Lists 				 :lu:
:PROPERTIES:
:SCAT: lu
:CUSTOM_ID: a1-stacks-implement-ll
:END:
  
** Linked Lists 						       :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: a1-stacks-implement-linked-list
:END:

*** Description 						       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-stacks-implement-linked-list-des
:END:
Stacks implemented using linked lists.

*** Interactive Stack 						       :reqa:
:PROPERTIES:
:SCAT: reqa
:TYPE: InteractiveJS
:CUSTOM_ID: a1-interactive-stack-linked-list-js
:END:
Stack implemented using linked lists along with push, pop buttons etc. 

** Time Complexity 						       :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: a1-interactive-stacks-ll-time
:END:

*** Time complexity 						       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-interactive-stacks-ll-time-compl
:END:
Time complexity for insertion, deletion, search.

* Applications of Stacks 						 :lu:
:PROPERTIES:
:SCAT: lu
:CUSTOM_ID: a1-stack-apps
:END:

** Applications 						       :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: a1-stack-apps-task
:END:

*** Stacks applications 					       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-stack-apps-txt
:END:
List out applications of stacks. Possible example include:
- Ask the user how they would reverse a word. Push the letters of the word into a stack and pop one by one. Time complexity O(n).  
- "Undoing" in text editors. This is done by pushing all changes into a stack and then popping them.
- Backtracking - for example a maze. When you reach a dead end, you'll need to go to previous stack point, so just save all choices onto stack and pop as you choose.
- Expression evaluation - conversion to infix, prefix, postfix and their evaluation. /How much depth to go into?/

* Quiz on Stacks 							 :lu:
:PROPERTIES:
:SCAT: lu
:CUSTOM_ID: a1-stacks-quiz
:END:

** Test 							       :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: a1-stacks-test
:END:

*** Questions 							       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-stacks-q-txt
:END:
Five questions for the post-test.

** End of Quiz 							       :task:
:PROPERTIES:
:SCAT: scene
:CUSTOM_ID: a1-end-of-stacks-quiz
:END:

*** Quiz Ending 						       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-end-of-stacks-quiz-des
:END:
Final score of quiz along with answers to the quiz.

* Thought Problems on Stacks 						 :lu:
:PROPERTIES:
:SCAT: lu
:CUSTOM_ID: a1-stacks-thought-probs
:END:
  
** Thought Problems 						       :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: a1-stacks-thought-probs-task
:END:

*** Types of thought problems 					       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-stacks-thought-probs-intro
:END:
Different types of thought problems to make the user think more about stacks.

* Introduction to Queues 						 :lu:
:PROPERTIES:
:SCAT: lu
:CUSTOM_ID: a1-queues-intro
:END:
 
** Real Life Example Of Queues 					       :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: a1-queues-example
:END:

*** Image of a queue of people 					       :inla:
:PROPERTIES:
:SCAT: inla
:URL: https://images.pexels.com/photos/51342/books-education-school-literature-51342.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260 
:CAPTION: Queue of people 
:CUSTOM_ID: a1-queues-example-image
:END:
A queue of people to visually represent queues data structure. Suitable image 
could not be found, so will need to be designed.

*** Queue of People 						       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-people-queue-example
:REFERENCE_ID: a1-queues-example-images
:END:
Explanation of example of queue of people. Introduce enqueue/dequeue by describing how people in the front of a queue are served first and people are added to end of the line.

** Queues Representation and Operations 			       :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: a1-queues-rep-and-op
:END:

*** Queues Data Structure 					       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-queues-ds
:END:
Introduction to queues.

*** Queues Data Structure Image 				       :reqa:
:PROPERTIES:
:SCAT: reqa
:TYPE: Image
:CUSTOM_ID: a1-queues-ds-image
:REFERENCE_ID: a1-queues-ds
:END:
 
*** Queue Operations 						       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-queues-ops
:END:
Queue operations and FIFO.

*** Queue operations image 					       :reqa:
:PROPERTIES:
:SCAT: reqa
:TYPE: Image
:CUSTOM_ID: a1-queues-ops-image
:REFERENCE_ID: a1-queues-ops
:END:
Images of enqueue and dequeue.

*** Queue Operations practice 					       :reqa:
:PROPERTIES:
:SCAT: reqa
:TYPE: InteractiveJS
:CUSTOM_ID: a1-interactive-queue-ops-js
:END:
Practice for the operations for queue.

*** Queue Operations exercise 					       :reqa:
:PROPERTIES:
:SCAT: reqa
:TYPE: InteractiveJS
:CUSTOM_ID: a1-interactive-queue-ops-exercise-js
:END:
Exercise to test the user.

* Types of Queues 							 :lu:
:PROPERTIES:
:SCAT: lu
:CUSTOM_ID: a1-queue-types
:END:

** Simple queue 						       :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: a1-simple-queue
:END:

*** Description 						       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-simple-queue-des
:END:
Description of simple queue.

*** Simple Queue Image 						       :inla:
:PROPERTIES:
:SCAT: inla
:URL: https://www.tutorialride.com/images/data-structures/simple-queue.jpeg
:CAPTION: Simple Queue
:CUSTOM_ID: a1-simple-queue-image
:REFERENCE_ID: a1-simple-queue-des
:END:
An image of simple queues. A suitable image, could not be found, so will need to be designed.
It should look like the image url provided, but with numbers inside the boxes.

** Circular queue 						       :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: a1-circular-queue
:END:

*** Description 						       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-circular-queue-des
:END:
Circular queue description.

*** Circular Queue Image 					       :inla:
:PROPERTIES:
:SCAT: inla
:URL: https://www.tutorialride.com/images/data-structures/circular-queue2.jpeg
:CAPTION: Circular Queue
:CUSTOM_ID: a1-circular-queue-image
:REFERENCE_ID: a1-circular-queue-des
:END:
An image of circular queues. A suitable image, could not be found, so will need to be designed.

** Priority queue 						       :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: a1-priority-queue
:END:

*** Description 						       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-priority-queue-des
:END:
Description of priority queue.

** Double Ended queue 						       :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: a1-de-queue
:END:
    
*** Description 						       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-de-queue-des
:END:
Description of double ended queue.

*** Double Ended Queue Image 					       :inla:
:PROPERTIES:
:SCAT: inla
:URL: https://www.tutorialride.com/images/data-structures/dequeue.jpeg
:CAPTION: Double Ended Queue
:CUSTOM_ID: a1-de-queue-image
:REFERENCE_ID: a1-de-queue-des
:END:
An image of double ended queues. A suitable image, could not be found, so will need to be designed.

* Implementation of Queues Using Arrays 				 :lu:
:PROPERTIES:
:SCAT: lu
:CUSTOM_ID: a1-queues-implement-array
:END:

** Arrays 							       :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: a1-queues-implement-arrays
:END:

*** Description 						       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-queues-implement-array-des   
:END:
Talk about how queues are implemeted using arrays.

*** Interactive Queue 						       :reqa:
:PROPERTIES:
:SCAT: reqa
:TYPE: InteractiveJS
:CUSTOM_ID: a1-interactive-queue-array-js
:END:
An empty queue implemented as an array (with size as entered by user)
is shown along with array indexes. Enqueue/dequeue buttons with values
to be enqueued as inserted by user.

** Time Complexity 						       :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: a1-interactive-queues-array-time
:END:

*** Time for Operations 					       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-interactive-queues-array-time-compl
:END:
Time for enqueue, dequeue and search.

* Implementation of Queues Using Linked Lists 				 :lu:
:PROPERTIES:
:SCAT: lu
:CUSTOM_ID: a1-queues-implement-ll
:END:

** Linked Lists 						       :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: a1-queues-implement-linked-list
:END:

*** Description 						       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-queues-implement-linked-list-des
:END:
Queues using linked lists.

*** Interactive Queue 						       :reqa:
:PROPERTIES:
:SCAT: reqa
:TYPE: InteractiveJS
:CUSTOM_ID: a1-interactive-queue-linked-list-js
:END:
Queues implemented using linked list is shown.

** Time Complexity 						       :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: a1-interactive-queues-ll-time
:END:

*** Time for Operations 					       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-interactive-queues-ll-time-compl
:END:
Time for enqueue, dequeue and search.

* Applications of Queues 						 :lu:
:PROPERTIES:
:SCAT: lu
:CUSTOM_ID: a1-queue-apps
:END:

** Applications 						       :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: a1-queue-apps-task
:END:

*** Queue applications 						       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-queue-apps-txt
:END:
Applications of queues. Possible examples include:
- Printers, where the first one to be entered is the first to be processed.
- Website, when there are many requests, queue is used to process first request.
- Software running operating system is implemented as multilevel priority queue.

* Quiz on Queues 							 :lu:
:PROPERTIES:
:SCAT: lu
:CUSTOM_ID: a1-queues-quiz
:END:

** Test 							       :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: a1-queues-test
:END:

*** Questions 							       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-queues-q-txt
:END:
Five questions for the post-test.

** End of Quiz 							       :task:
:PROPERTIES:
:SCAT: scene
:CUSTOM_ID: a1-end-of-queues-quiz
:END:

*** Quiz Ending 						       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-end-of-queues-quiz-des
:END:
Final score of quiz along with answers to the quiz.

* Thought Problems on Queues 						 :lu:
:PROPERTIES:
:SCAT: lu
:CUSTOM_ID: a1-queues-thought-probs
:END:

** Thought Problems 						       :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: a1-queues-thought-probs-task
:END:

*** Types of thought problems 					       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: a1-queues-thought-probs-intro
:END:
Different types of thought problems to make the user think more about queues.

